CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Support
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

 INTRODUCTION
-------------

The Content Moderation Revision Delete module allows administrators to prune
draft revisions from nodes in order to prevent very large databases. It is
similar to the Node Revisions Delete module, but it is intended to be used on
sites using Content Moderation. It has the following differences from Node
Revision Delete:

    * Builds its query based on the content_moderation_state_field_revision
      table (no joins) instead of the node_field_revision table with joins;
    * It handles multilingual sites;
    * It removes all revisions EXCEPT published, the
      getLatestTranslationAffectedRevisionId and any forward revisions;
    * It only works on published nodes, assuming non-published nodes may
      be in a workflow still and diffs of the previous revisions may be
      required.

 Support
--------

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/cm_revision_delete

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/cm_revision_delete

REQUIREMENTS
------------

This module does not requires any modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Administer Content Moderation Revision Delete

     Allows access to the configuration form to manage the module's settings.

 * To configure the module's settings go to Administration » Configuration »
   Content authoring » Content Moderation Revision Delete.

MAINTAINERS
-----------

Current maintainers:
 * Joel Brockbank (joel_osc) - https://www.drupal.org/u/joel_osc
 * Stephen Mulvihill (smulvih2) - https://www.drupal.org/u/smulvih2

This project has been sponsored by:
 * OpenPlus
   With over a decade of trusted expertise, our team of professionals
   have un-matched experience creating content architecture that works.
   We are a team of experienced digital professionals with a passion for
   enterprise content management solutions.
   Visit https://www.openplus.ca for more information.
