<?php

namespace Drupal\cm_revision_delete\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NodeRevisionDeleteAdminSettingsForm.
 *
 * Admin config form definition.
 *
 * @package Drupal\node_revision_delete\Form
 */
class DevelForm extends ConfigFormBase {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language_manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module_handler service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface|null $typedConfigManager,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LanguageManagerInterface $languageManager,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cm_revision_delete.devel',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cm_revision_delete_devel';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cm_revision_delete.devel');

    if (!empty($config->get('nid'))) {
      $revisions_table = $this->getRevisionsToDelete($config);
      if ($revisions_table) {
        $form['revisions_table'] = $revisions_table;
      }
    }

    $langcodes = $this->languageManager->getLanguages();
    $languages = array_keys($langcodes);
    $lang_options = [];

    foreach ($languages as $lang) {
      $lang_options[$lang] = $lang;
    }

    $form['devel'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Devel settings'),
      '#description' => $this->t('This form allows you to test the revision candidate algorithm. The revisions returned are candidates for deletion.'),
    ];

    $form['devel']['nid'] = [
      '#type' => 'number',
      '#title' => $this->t('Node ID'),
      '#description' => $this->t('Enter a nid to test.'),
      '#default_value' => $config->get('nid'),
      '#required' => TRUE,
    ];

    $form['devel']['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => $lang_options,
      '#default_value' => $config->get('language'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cm_revision_delete.devel')
      ->set('nid', $form_state->getValue('nid'))
      ->set('language', $form_state->getValue('language'))
      ->save();
  }

  /**
   * Sets messages with the revisions to delete.
   *
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   *   The configuration.
   *
   * @return array|null
   *   A render array or NULL if the node is NOT a candidate for processing.
   */
  public function getRevisionsToDelete($config): array|null {
    $settings_config = $this->config('cm_revision_delete.settings');
    $nid = $config->get('nid');
    $lang = $config->get('language');
    $limit = $settings_config->get('cm_revision_delete_revisions');

    $revisions = cm_revision_delete_get_revisions($nid, $lang, $limit);
    $is_node_candidate = cm_revision_delete_get_node_candidate($nid);

    $revision_array = [];
    $this->messenger()->deleteAll();

    if (!empty($is_node_candidate)) {
      $this->messenger()->addMessage('This node is a candidate for processing.');

      foreach ($revisions as $vid) {
        $revision = $this->entityTypeManager->getStorage('node')->loadRevision($vid);

        if ($revision->hasTranslation($lang)) {
          $revision = $revision->getTranslation($lang);
        }

        $revision_array[] = [$vid, $revision->get('moderation_state')->getString()];
      }

      krsort($revision_array);

      return [
        '#type' => 'table',
        '#header' => [
          $this->t('Revision ID'),
          $this->t('Moderation state'),
        ],
        '#rows' => $revision_array,
        '#empty' => $this->t('No revisions to show.'),
      ];
    }
    else {
      $this->messenger()->addMessage('This node is NOT a candidate for processing.');
    }

    return NULL;
  }

}
