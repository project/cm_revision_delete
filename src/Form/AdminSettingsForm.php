<?php

namespace Drupal\cm_revision_delete\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NodeRevisionDeleteAdminSettingsForm.
 *
 * Admin config form definition.
 *
 * @package Drupal\node_revision_delete\Form
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cm_revision_delete.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cm_revision_delete_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cm_revision_delete.settings');

    $options_node_revision_delete_time = [
      '-1'       => $this->t('Never'),
      '0'        => $this->t('Every time cron runs'),
      '3600'     => $this->t('Every hour'),
      '86400'    => $this->t('Everyday'),
      '604800'   => $this->t('Every week'),
      '864000'   => $this->t('Every 10 days'),
      '1296000'  => $this->t('Every 15 days'),
      '2592000'  => $this->t('Every month'),
      '7776000'  => $this->t('Every 3 months'),
      '15552000' => $this->t('Every 6 months'),
      '31536000' => $this->t('Every year'),
      '63072000' => $this->t('Every 2 years'),
    ];
    $form['cm_revision_delete_time'] = [
      '#type' => 'select',
      '#title' => $this->t('How often should revisions be deleted when cron runs?'),
      '#description' => $this->t('Frequency of the scheduled mass revision deletion.'),
      '#options' => $options_node_revision_delete_time,
      '#default_value' => $config->get('cm_revision_delete_time'),
    ];

    $form['cm_revision_delete_revisions'] = [
      '#type' => 'number',
      '#title' => $this->t('How many revisions do you want to prune per cron run?'),
      '#description' => $this->t('Deleting node revisions is a database intensive task. Increase this value if you think that the server can handle more deletions per cron run.'),
      '#default_value' => $config->get('cm_revision_delete_revisions'),
      '#min' => 1,
      '#max' => 500,
    ];

    $form['published_states'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Workflow states to consider as published'),
      '#description' => $this->t('Enter a comma-separated list of workflow states to consider as published. This is useful if you have a state like archived that should also be considered as published.'),
      '#default_value' => $config->get('published_states') ? $config->get('published_states') : 'published',
      '#required' => TRUE,
    ];

    $form['maximum_revisions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Maximum number of published revisions to keep'),
    ];

    $form['maximum_revisions']['limit_revisions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Limit the amount of published revisions'),
      '#default_value' => $config->get('limit_revisions'),
    ];

    $form['maximum_revisions']['maximum_revisions_to_keep'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of published revisions to keep'),
      '#description' => $this->t('Set how many published revisions to keep, not including the latest default revision (current published revision). The oldest published revisions will be deleted when the total amount surpasses this value. Set it to 0 to remove all revisions older than the latest default revision.'),
      '#default_value' => $config->get('maximum_revisions_to_keep') ? $config->get('maximum_revisions_to_keep') : 0,
      '#min' => 0,
      '#max' => 20,
      '#states' => [
        'visible' => [
          ':input[name="limit_revisions"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cm_revision_delete.settings')
      ->set('cm_revision_delete_time', $form_state->getValue('cm_revision_delete_time'))
      ->set('cm_revision_delete_revisions', $form_state->getValue('cm_revision_delete_revisions'))
      ->set('published_states', $form_state->getValue('published_states'))
      ->set('limit_revisions', $form_state->getValue('limit_revisions'))
      ->set('maximum_revisions_to_keep', $form_state->getValue('maximum_revisions_to_keep'))
      ->save();
  }

}
